const fs = require("fs");

function problem2(){
    fs.readFile('../lipsum.txt','utf-8',(error,data) =>{
        if (error){
            console.log(error);
        }else{
            const answer = data.toUpperCase()
            const path = "../answer2/"
            const upperCase = "uppercase.txt"
            fs.mkdir("../answer2", function(){
                fs.appendFile(`${path}${upperCase}`,answer,(error)=>{
                    if (error){
                        console.log(error)
                    }else{
                        console.log("directory created")
                        fs.appendFile("./filenames.txt",`${upperCase}`,(error)=>{
                            if (error){
                                console.log(error)
                            }
                            console.log("uppercase file created")
                        })
                        const lower = answer.toLowerCase()
                        const path = "../answer2/"
                        const splitted = lower.slice(0, lower.length - 1).split(".").join(`*`)
                        const lowerCase = "lowercase.txt"
                        fs.appendFile(`${path}${lowerCase}`, splitted, (error) =>{
                            if (error) {
                                console.log(error)
                            }else{
                                console.log("lowercase file created");
                                fs.appendFile("./filenames.txt", `*${lowerCase}`, (error) =>{
                                    if (error){
                                        console.log(error)
                                    }else{
                                        fs.readFile(`${path}${upperCase}`, "utf-8", (error, data1) =>{
                                            if (error) {
                                                console.log(error)
                                            }else{
                                                const upper = data1;
                                                    fs.readFile(`${path}${lowerCase}`, "utf-8", (error, data2) => {
                                                        if (error) {
                                                            console.log(error)
                                                        }else{
                                                            const lower = data2;
                                                            const sortPath = "sorted.txt"
                                                            const concatenated = data1 + data2
                                                            const final = (concatenated.split(" ").join("").split("*").join("").split("").sort().join(""))
                                                            fs.appendFile(`${path}${sortPath}`, final, (error) => {
                                                                if (error) {
                                                                    console.log(error)
                                                                }else{
                                                                    fs.appendFile("./filenames.txt", `*${sortPath}`, (error) => {
                                                                        if (error) {
                                                                            console.log(error)
                                                                        }else{
                                                                            fs.readFile("./filenames.txt", "utf-8", (error, data) => {
                                                                                if (error) {
                                                                                    console.log(error)
                                                                                }else{
                                                                                    const fileNames = data.split("*")

                                                                                    fileNames.map(file =>{
                                                                                        fs.unlink(`${path}${file}`, (error) => {
                                                                                            console.log(`Deleted ${file}`)
                                                                                            if (error) {
                                                                                                console.log(error)
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }

                                                            })

                                                        }
                                                        
                                                    })
                                            }
                                        })
                                    }
                                })
                            }

                        })
                    }
                })
            })
        }
    })
}

module.exports = problem2