const fs = require("fs")
const data = "./sample.json"

function problem1() {
    fs.readFile(data, "utf-8", (error, file) => {
        if (error) {
            console.log(error)
        } else {
            try {
                fs.mkdir("../answer", function (error) {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log("answers directory created")
                        fs.writeFile("../answers/problem1.json",file, (error) => {
                            if (error) {
                                console.log(error)
                            }
                            else {
                                console.log("file created")
                                fs.unlink("../answers/problem1.json", function (error) {
                                    if (error) {
                                        console.log(error)
                                    } else {
                                        console.log("file deleted")
                                    }
                                })
                            }
                        })
                    }
                })

            } catch (error) {
                console.log(error)
            }
        }
    })
}

module.exports = problem1

